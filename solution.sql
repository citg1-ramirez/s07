-- Charmaine Mae I. Ramirez

-- The blog_db database was already created from the previous activity, so I just utilized it.
USE blog_db;

SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| posts             |
| posts_comments    |
| posts_likes       |
| users             |
+-------------------+

DESCRIBE users;
+------------------+--------------+------+-----+---------+----------------+
| Field            | Type         | Null | Key | Default | Extra          |
+------------------+--------------+------+-----+---------+----------------+
| id               | int(11)      | NO   | PRI | NULL    | auto_increment |
| email            | varchar(100) | NO   |     | NULL    |                |
| password         | varchar(300) | NO   |     | NULL    |                |
| datetime_created | datetime     | NO   |     | NULL    |                |
+------------------+--------------+------+-----+---------+----------------+

DESCRIBE posts;
+------------------+---------------+------+-----+---------+----------------+
| Field            | Type          | Null | Key | Default | Extra          |
+------------------+---------------+------+-----+---------+----------------+
| id               | int(11)       | NO   | PRI | NULL    | auto_increment |
| author_id        | int(11)       | NO   | MUL | NULL    |                |
| title            | varchar(500)  | NO   |     | NULL    |                |
| content          | varchar(5000) | NO   |     | NULL    |                |
| datetime_created | datetime      | NO   |     | NULL    |                |
+------------------+---------------+------+-----+---------+----------------+

DESCRIBE post_comments;
+--------------------+---------------+------+-----+---------+----------------+
| Field              | Type          | Null | Key | Default | Extra			 |
+--------------------+---------------+------+-----+---------+----------------+
| id                 | int(11)       | NO   | PRI | NULL    | auto_increment |
| post_id            | int(11)       | NO   | MUL | NULL    |				 |
| user_id            | int(11)       | NO   | MUL | NULL    |				 |
| content            | varchar(5000) | YES  |     | NULL    |				 |
| datetime_commented | datetime      | NO   |     | NULL    |				 |
+--------------------+---------------+------+-----+---------+----------------+

DESCRIBE post_likes;
+---------------+----------+------+-----+---------+----------------+
| Field         | Type     | Null | Key | Default | Extra          |
+---------------+----------+------+-----+---------+----------------+
| id            | int(11)  | NO   | PRI | NULL    | auto_increment |
| post_id       | int(11)  | NO   | MUL | NULL    |                |
| user_id       | int(11)  | NO   | MUL | NULL    |                |
| datetime_like | datetime | NO   |     | NULL    |                |
+---------------+----------+------+-----+---------+----------------+



-- 3. Add the following records to the blog_db database
INSERT INTO users (email, password, datetime_created) VALUES('johnsmith@gmail.com', 'passwordA', '2021-01-01 01:00:00');
INSERT INTO users (email, password, datetime_created) VALUES('johnsmith@gmail.com', 'passwordA', '2021-01-01 01:00:00');
INSERT INTO users (email, password, datetime_created) VALUES('janesmith@gmail.com', 'passwordC', '2021-01-01 03:00:00');
INSERT INTO users (email, password, datetime_created) VALUES('mariadelacruz@gmail.com', 'passwordD', '2021-01-01 04:00:00');
INSERT INTO users (email, password, datetime_created) VALUES('johndoe@gmail.com', 'passwordE', '2021-01-01 05:00:00');

SELECT * FROM users;
+----+-------------------------+-----------+---------------------+
| id | email                   | password  | datetime_created    |
+----+-------------------------+-----------+---------------------+
|  1 | johnsmith@gmail.com     | passwordA | 2021-01-01 01:00:00 |
|  2 | juandelacruz@gmail.com  | passwordB | 2021-01-01 02:00:00 |
|  3 | janesmith@gmail.com     | passwordC | 2021-01-01 03:00:00 |
|  4 | mariadelacruz@gmail.com | passwordD | 2021-01-01 04:00:00 |
|  5 | johndoe@gmail.com       | passwordE | 2021-01-01 05:00:00 |
+----+-------------------------+-----------+---------------------+


INSERT INTO posts (author_id, title, content, datetime_created) VALUES (1, 'First Code', 'Hello World!', '2021-01-02 01:00:00');
INSERT INTO posts (author_id, title, content, datetime_created) VALUES (1, 'Second Code', 'Hello Earth!', '2021-01-02 02:00:00');
INSERT INTO posts (author_id, title, content, datetime_created) VALUES (2, 'Third Code', 'Welcome to Mars!', '2021-01-02 03:00:00');
INSERT INTO posts (author_id, title, content, datetime_created) VALUES (4, 'Fourth Code', 'Bye bye solar system!', '2021-01-02 04:00:00');

SELECT * FROM posts;
+----+-----------+-------------+-----------------------+---------------------+
| id | author_id | title       | content               | datetime_created    |
+----+-----------+-------------+-----------------------+---------------------+
|  1 |         1 | First Code  | Hello World!          | 2021-01-02 01:00:00 |
|  2 |         1 | Second Code | Hello Earth!          | 2021-01-02 02:00:00 |
|  3 |         2 | Third Code  | Welcome to Mars!      | 2021-01-02 03:00:00 |
|  4 |         4 | Fourth Code | Bye bye solar system! | 2021-01-02 04:00:00 |
+----+-----------+-------------+-----------------------+---------------------+

-- 4. Get all the post with an author ID of 1.
 SELECT * FROM posts WHERE author_id = 1;
+----+-----------+-------------+--------------+---------------------+
| id | author_id | title       | content      | datetime_created    |
+----+-----------+-------------+--------------+---------------------+
|  1 |         1 | First Code  | Hello World! | 2021-01-02 01:00:00 |
|  2 |         1 | Second Code | Hello Earth! | 2021-01-02 02:00:00 |
+----+-----------+-------------+--------------+---------------------+

-- 5. Get all the user's email and datetime of creation.
 SELECT email, datetime_created FROM users;
+-------------------------+---------------------+
| email                   | datetime_created    |
+-------------------------+---------------------+
| johnsmith@gmail.com     | 2021-01-01 01:00:00 |
| juandelacruz@gmail.com  | 2021-01-01 02:00:00 |
| janesmith@gmail.com     | 2021-01-01 03:00:00 |
| mariadelacruz@gmail.com | 2021-01-01 04:00:00 |
| johndoe@gmail.com       | 2021-01-01 05:00:00 |
+-------------------------+---------------------+

-- 6. Update a post's content to Hello to the people of the Earth! where its initial content is Hello Earth! uby using the record's ID.
UPDATE posts SET content = 'Hello to the people of the Earth!' where id = 2;
SELECT * FROM posts WHERE id =2;
+----+-----------+-------------+-----------------------------------+---------------------+
| id | author_id | title       | content                           | datetime_created    |
+----+-----------+-------------+-----------------------------------+---------------------+
|  2 |         1 | Second Code | Hello to the people of the Earth! | 2021-01-02 02:00:00 |
+----+-----------+-------------+-----------------------------------+---------------------+

-- 7. Delete the user with an email of johndoe@gmail.com
DELETE FROM users WHERE email = 'johndoe@gmail.com';
SELECT * FROM users;
+----+-------------------------+-----------+---------------------+
| id | email                   | password  | datetime_created    |
+----+-------------------------+-----------+---------------------+
|  1 | johnsmith@gmail.com     | passwordA | 2021-01-01 01:00:00 |
|  2 | juandelacruz@gmail.com  | passwordB | 2021-01-01 02:00:00 |
|  3 | janesmith@gmail.com     | passwordC | 2021-01-01 03:00:00 |
|  4 | mariadelacruz@gmail.com | passwordD | 2021-01-01 04:00:00 |
+----+-------------------------+-----------+---------------------+



